#pragma once

#include <string>
using std::string;

#include <SFML/Graphics.hpp>

#include "GUI/Text.h"
#include "States/State.h"

/**
 * Fills the DataBuffer with junxions coming from a given GeoJSON file.
 * The rule is the following: junxion comes as a `GeometryCollection` and contains;
 * - a single `Point` which represents the junxion's node location;
 * - one or more adjacents as a `MultilineString` list of locations.
 * 
 * @param geojson_file    The input file
 * @param bbox            Pointer to a `osmium::Box` extended to include spatial extent of input data
 * @param min_connections Reference to track the minimum of junxions' connections
 * @param max_connections Reference to track the maximum of junxions' connection.
 */

namespace sen {
class WalkingState : public State {
  Text message_text;

 public:
  WalkingState(const string& message,
               const unsigned font_size = 35u) : message_text(message) {
    const sf::Vector2f& init_size = (sf::Vector2f)Application::getInitialWindowSize();

    message_text.setPosition(init_size * .5f);
    message_text.setCharacterSize(font_size);
  }

  virtual void render(sf::RenderTarget& target) override {
    message_text.render(target);
  }
};
}  // namespace sen