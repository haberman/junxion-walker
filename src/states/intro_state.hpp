#pragma once

#include <future>  // for await/async
#include <string>
#include <thread>  // for await/async
using std::string;

#include <SFML/Graphics.hpp>
#include <osmium/geom/mercator_projection.hpp>
#include <osmium/osm/box.hpp>

#include "GUI/Text.h"
#include "States/State.h"

#include "../io/data_buffer.hpp"
#include "../states/walking_state.hpp"
#include "../utils.hpp"

/**
 * Fills the DataBuffer with junxions coming from a given GeoJSON file.
 * The rule is the following: junxion comes as a `GeometryCollection` and contains;
 * - a single `Point` which represents the junxion's node location;
 * - one or more adjacents as a `MultilineString` list of locations.
 * 
 * @param geojson_file    The input file
 * @param bbox            Pointer to a `osmium::Box` extended to include spatial extent of input data
 * @param min_connections Reference to track the minimum of junxions' connections
 * @param max_connections Reference to track the maximum of junxions' connection.
 */

namespace sen {
class IntroState : public State {
  Text message_text;
  Text info_text;

 public:
  IntroState(const string& file,
             const string& message,
             const string& info = "",
             const unsigned font_size = 35u)
      : message_text(message), info_text(info) {
    const sf::Vector2f& init_size = (sf::Vector2f)Application::getInitialWindowSize();

    message_text.setPosition(init_size * .5f);
    message_text.setCharacterSize(font_size);

    info_text.setPosition({init_size.x * .5f, init_size.y * .5f + font_size + 5u});
    info_text.setCharacterSize(font_size - 10u);

    DataBuffer* data_buffer = DataBuffer::get_instance();
    data_buffer->parse_junxions(file);
  }

  virtual void update(float delta_time, sf::RenderWindow& window) override {
    DataBuffer* data_buffer = DataBuffer::get_instance();

    const bool loaded = Utils::is_future_done<bool>(data_buffer->bool_future);
    if (loaded) {
      auto intro = std::make_unique<sen::WalkingState>("Walking");
      sen::StateManager::pushState(std::move(intro));
    } else {
    }
  }

  virtual void render(sf::RenderTarget& target) override {
    message_text.render(target);
    info_text.render(target);
  }
};
}  // namespace sen