#include <unistd.h>  // for posix access()
#include <cstdlib>   // for exit codes
#include <iostream>  // for cout cerr endl

#include <SFML/Graphics.hpp>

#include "Application.h"
#include "Managers/CacheSystem.h"
#include "Managers/StateManager.h"

#include "io/input_parser.hpp"
#include "states/intro_state.hpp"
#include "utils.hpp"

using std::string;

const string PGM_NAME = "junXion walker 0.1";
const string PGM_EXEC = "jxn_walker";
const string PGM_DESCRIPTION = "Computing agents walking on a geodata metric system";

const string DEFAULT_GEOJSON = "res/Json/brussels_junxions-7.geojson";

int main(int argc, char** argv) {
  /** Arguments setup. */
  InputParser input{argc, argv};

  if (input.argument_exists("-h")) {
    std::cout << PGM_NAME << std::endl
              << std::endl;
    std::cout << "Usage: " << PGM_EXEC << "[options] junxions.geojson\n";
    std::cout << "_______\n";
    std::cout << "Options\n"
              << std::endl;
    std::cout << "* generic *\n";
    std::cout << "  -h\tshow this help message and exit;\n";
    std::cout << "  -v\tincrease verbosity.\n";
    std::cout << "* behavior *\n";
    std::cout << "  -a\tnumber of walking agents [default: 2];\n";

    return EXIT_SUCCESS;
  }

  string geojson_input = input.get_argument();
  if (geojson_input.empty()) {
    geojson_input = DEFAULT_GEOJSON;
  }

  /** Common rejections. */
  if (geojson_input.find(".geojson") == string::npos) {
    std::cerr << "[ERROR] Missing required geojson file input." << std::endl;
    return EXIT_FAILURE;
  }

  if (access(geojson_input.c_str(), F_OK) == -1) {
    std::cerr << "[ERROR] File " << geojson_input << " does not exists." << std::endl;
    return EXIT_FAILURE;
  }

  /** Junxions parser. */
  // DataBuffer* data_buffer = DataBuffer::get_instance();

  sf::RenderWindow window(sf::VideoMode(1280, 720), PGM_NAME /*, sf::Style::Fullscreen*/);
  window.setFramerateLimit(0U);

  Application::init(&window);

  auto intro = std::make_unique<sen::IntroState>(geojson_input, PGM_NAME, "parsing json");
  sen::StateManager::switchBackButton(false);
  sen::StateManager::pushState(std::move(intro));

  Application::run();

  // data_buffer->texture_space = new IntRect(0, 0, output_width, output_height);

  // data_buffer->pixel = new Vector2f(2.f / (float)data_buffer->texture_space->width, 2.f / (float)data_buffer->texture_space->height);
  // data_buffer->radius = MARKER_RADIUS;

  /** Arguments setup. */
  // const string& name = Utils::basename(geojson_input);
  // const bool verbose = input.get_argument("-v", false);
  // const unsigned int agents = input.get_argument("-a", 2);
}