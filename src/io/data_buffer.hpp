#pragma once

#include <algorithm>      // For adjacent_find
#include <iostream>       // For cout
#include <map>            // For junxions
#include <unordered_set>  // For unique_nodes

// OSM types mapping.
#include <osmium/geom/coordinates.hpp>
#include <osmium/osm/location.hpp>

#include <SFML/Graphics.hpp>

using std::string;
using std::vector;

const unsigned DEFAULT_WINDOW_WIDTH = 1280;

/** @addtogroup io
 *  @{
 */

/**
 * Holds an OSM node as a transactional piece of mulitple projections.
 */
struct Point {
 public:
  /**
   * Empty constructor.
   * Here only to initialize `mercator` value.
   */
  // Point() noexcept: mercator({}) {}

  /** The `OSM` node id. */
  uint64_t uid;

  /** Lat/Lng location. */
  osmium::Location location;

  /** Mercator projection. */
  osmium::geom::Coordinates mercator;

  /** -1/1 projection. */
  sf::Vector2f unit;

  /** pixel projection. */
  sf::Vector2i texture;
};

/**
 * Holds two lists containing nodes being either before or after a junxion.
 * @fixme the way we identify an adjacent using a `way_id` is wrong since we could have multiple ajdacents on a single way.
 */
struct Adjacent {
 public:
  uint64_t way_uid;
  vector<Point> after;
  vector<Point> before;
};

/**
 * Describes a junxion, which is sort of an augmented regular OSM `node`.
 */
class Junxion {
 public:
  /**
   * Empty constructor
   */
  Junxion() {}

  /**
   * Constructs a new `Junxion` with an initial node id.
   * @param uid OSM node id
   */
  Junxion(const uint64_t uid);

  /**
   * Constructs a new `Junxion` with a complete OSM node.
   * @param uid OSM node id
   */
  Junxion(const uint64_t uid, const osmium::Location location);

  /**
   * Prints informations about the `Junxion` content and state.
   */
  void print();

  unsigned connections = 1u;
  float weight = 0.f;

  Point point;
  std::map<uint64_t, Adjacent> adjacents;

  Junxion& operator=(const Junxion&);
};

/**
 * Singleton class that keeps pointers to chunks of memory used a little bit everywhere.
 */
class DataBuffer {
 public:
  /** Size of a pixel expressed as a fraction of (-1/1) screen space. */
  sf::Vector2f* pixel;
  /** Size of a radius (in pixels) used to scale some 2D shapes. */
  float radius;
  /** Returns the vector of `pixel * radius`. */
  const sf::Vector2f pixel_radius() const;

  /** A map for node ids of collected junxions. */
  std::map<uint64_t, Junxion> junxions;
  /** Indicates if we have valid data to start walking on. */
  bool holds_valid_junxions = false;
  /** Parse junxions out of a given geojson file  */
  void parse_junxions(const string&);

  const sf::IntRect* texture_space;
  const sf::FloatRect* mercator_space;
  const sf::FloatRect* unit_space;

  std::future<bool> bool_future;

  /** Returns the singleton. */
  static DataBuffer* get_instance();

  /** Frees the `intance_` pointer. */
  void destroy_instance();

 private:
  /** The singleton instance. */
  static DataBuffer* instance_;

  /** Private constructor. */
  DataBuffer() = default;

  /** Private destructor. */
  virtual ~DataBuffer();
};

/**@}*/