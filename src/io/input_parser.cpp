#include "input_parser.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>

using std::string;
using std::stringstream;

InputParser::InputParser(int& argc, char** argv) {
  for (int i = 1; i < argc; ++i) {
    this->tokens_.push_back(string(argv[i]));
  }
}

const unsigned int InputParser::get_argument(const string& option, const int i) const {
  vector<string>::const_iterator itr = find_option(option);

  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    stringstream string_value;
    string_value << *itr;

    int int_value;
    string_value >> int_value;

    return int_value;
  }

  return i;
}

const bool InputParser::get_argument(const string& option, const bool b) const {
  vector<string>::const_iterator itr = find_option(option);

  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    return true;
  } else {
    return false;
  }

  return b;
}

const string& InputParser::get_argument(const string& option, const string& s) const {
  vector<string>::const_iterator itr = find_option(option);

  if (itr != this->tokens_.end() && ++itr != this->tokens_.end()) {
    return *itr;
  } else if (!s.empty()) {
    return s;
  }

  static const string empty_string("");
  return empty_string;
}

/**
 * Returns last token.
 **/
const string& InputParser::get_argument() const {
  if (tokens_.empty()) {
    static const string empty_string("");
    return empty_string;
  }

  return tokens_.back();
}

const bool InputParser::argument_exists(const string& option) const {
  return find(this->tokens_.begin(), this->tokens_.end(), option) != this->tokens_.end();
}

vector<string>::const_iterator InputParser::find_option(const string& option) const {
  vector<string>::const_iterator itr;
  return find(this->tokens_.begin(), this->tokens_.end(), option);
}
