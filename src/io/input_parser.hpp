#pragma once

#include <string>
#include <vector>

using std::string;
using std::vector;

/** @addtogroup io
 *  @{
 */
class InputParser {
 public:
  InputParser(int& argc, char** argv);

  const string& get_argument() const;
  const string& get_argument(const string& option, const string& s = "") const;
  const bool get_argument(const string& option, const bool b) const;
  const unsigned int get_argument(const string& option, const int i) const;

  const bool argument_exists(const string& option) const;

 private:
  vector<string> tokens_;
  vector<string>::const_iterator find_option(const string& option) const;
};

/**@}*/