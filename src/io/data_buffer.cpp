
#include <fstream>  // for loading geojson
#include <future>   // for await/async

#include <json/json.h>
#include <osmium/geom/mercator_projection.hpp>
#include <osmium/osm/box.hpp>

#include "Application.h"
#include "data_buffer.hpp"

Junxion::Junxion(const uint64_t uid) {
  point.uid = uid;
  point.location = osmium::Location();
}

Junxion::Junxion::Junxion(const uint64_t uid, const osmium::Location location) {
  point.uid = uid;
  point.location = location;
}

Junxion& Junxion::operator=(const Junxion& junxion) {
  point.uid = junxion.point.uid;
  point.location = junxion.point.location;
  connections = junxion.connections;

  return *this;
}

void Junxion::print() {
  std::cout << "Junxion " << point.uid << std::endl;
  std::cout << "  -> location:   [" << point.location.lon() << ", " << point.location.lat() << "]" << std::endl;
  std::cout << "  -> mercator:   [" << point.mercator.x << ", " << point.mercator.y << "]" << std::endl;
  std::cout << "  -> unit: [" << point.unit.x << ", " << point.unit.y << "]" << std::endl;
  std::cout << "  -> connections:      " << connections << std::endl;
  std::cout << "  -> adjacents amount: " << adjacents.size() << std::endl;
}

DataBuffer* DataBuffer::instance_ = nullptr;

const sf::Vector2f DataBuffer::pixel_radius() const {
  const sf::Vector2f v(pixel->x * radius, pixel->y * radius);
  return v;
}

void DataBuffer::parse_junxions(const string& file) {
  bool_future = std::async(std::launch::async, [this, file]() -> bool {
    osmium::Box bbox;
    float min_connections = 100.f;
    float max_connections = 0.f;

    Json::CharReaderBuilder builder;
    builder["collectComments"] = false;
    Json::Value geojson;

    /** Feed the reader with an input stream */
    std::ifstream stream(file);

    const bool ok = parseFromStream(builder, stream, &geojson, nullptr);
    if (!ok) {
      std::cerr << "[ERROR] Failed at parsing geojson file: " << file << std::endl;
      return false;
    }

    /** The root GeoJSON features list */
    const Json::Value& features = geojson["features"];

    for (unsigned int i = 0; i < features.size(); ++i) {
      /** We're at junxion's level. */
      const Json::Value& geometries = features[i]["geometry"]["geometries"];
      const Json::Value& properties = features[i]["properties"];

      const Json::UInt connections = properties["connections"].asUInt();
      const Json::UInt64 uid = properties["uid"].asUInt64();

      for (unsigned short j = 0; j < geometries.size(); ++j) {
        const Json::Value& geometry = geometries[j];

        /** The type is a `Point` -> we store a new Junxion instance in the DataBuffer singleton. */
        if (geometry["type"].asString() == "Point") {
          const float loc_lng = geometry["coordinates"][0].asFloat();
          const float loc_lat = geometry["coordinates"][1].asFloat();

          Junxion junxion(uid, osmium::Location(loc_lng, loc_lat));
          junxion.connections = connections;
          bbox.extend(junxion.point.location);

          DataBuffer::get_instance()->junxions[uid] = junxion;
        }
        /** The type is a `MultiLineString` -> we append points on either the `after` or `before` list of junxion's adjacents. */
        else if (geometry["type"].asString() == "LineString") {
          const Json::Value& multiline = geometry["coordinates"];
          const Json::Value& way_uid = geometry["properties"]["uid"];
          const Json::Value& side = geometry["properties"]["side"];

          Adjacent adjacent;
          adjacent.way_uid = way_uid.asInt64();

          for (unsigned short k = 0; k < multiline.size(); ++k) {
            Point point;
            point.location = osmium::Location(multiline[k][0].asFloat(),
                                              multiline[k][1].asFloat());

            if (side.asString() == "before") {
              adjacent.before.push_back(point);
            } else if (side.asString() == "after") {
              adjacent.after.push_back(point);
            }

            junxions[uid].adjacents[adjacent.way_uid] = adjacent;
          }
        }
      }

      /** Updates the min/max amount of junxions' connections. */
      min_connections = std::min(min_connections, (float)connections);
      max_connections = std::max(max_connections, (float)connections);
    }

    using osmium::geom::Coordinates;
    const Coordinates merc_tr = osmium::geom::lonlat_to_mercator(bbox.top_right());
    const Coordinates merc_bl = osmium::geom::lonlat_to_mercator(bbox.bottom_left());

    const double merc_width = abs(merc_tr.x - merc_bl.x);
    const double merc_height = abs(merc_tr.y - merc_bl.y);

    const unsigned screen_width = Application::getWindow().getSize().x;
    const unsigned output_height = DEFAULT_WINDOW_WIDTH * (merc_width / merc_height);

    mercator_space = new sf::FloatRect(merc_bl.x, merc_bl.y, merc_width, merc_height);
    texture_space = new sf::IntRect(0, 0, screen_width, output_height);
    pixel = new sf::Vector2f(2.f / (float)texture_space->width, 2.f / (float)texture_space->height);

    return true;
  });

  // data_buffer->radius = MARKER_RADIUS;
}

DataBuffer::~DataBuffer() {
  delete mercator_space;
  delete texture_space;
  delete unit_space;
  delete pixel;
}

DataBuffer* DataBuffer::get_instance() {
  if (!instance_) {
    instance_ = new DataBuffer;
    instance_->unit_space = new sf::FloatRect({-1.f, -1.f}, {2.f, 2.f});
  }

  return instance_;
}

void DataBuffer::destroy_instance() {
  if (instance_) {
    delete instance_;
  }
}
