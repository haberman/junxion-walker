#pragma once

#include <chrono>
#include <future>

#include <string>
using std::string;

class Utils {
 public:
  /**
   * Extracts a file's basename from a given path.
   * 
   * @param path The input path to inspect
   * @param ext  Whether we should include the file extension as well
   * @return The file's basename.
   */
  static const string basename(const string& path, const bool ext = true) {
    string filename = path;

    const size_t last_slash_idx = path.find_last_of("\\/");
    if (string::npos != last_slash_idx) {
      filename.erase(0, last_slash_idx + 1);
    }

    if (!ext) {
      const size_t period_idx = filename.rfind('.');
      if (string::npos != period_idx) {
        filename.erase(period_idx);
      }
    }

    return filename;
  }

  template <typename R>
  static bool is_future_done(std::future<R> const& f) {
    return f.wait_for(std::chrono::seconds(0)) == std::future_status::ready;
  }
};