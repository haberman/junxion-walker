# JUNXION - WALKER
Computing agents walking on a geodata metric system

## junXion walker 0.1
#### `junXion walker [OPTION...]`
```
options:
  -h, --help        show this help message and exit;
  -v, --verbose     increase verbosity;
  -a, --agents arg  number of walking agents [default: 2];
```
The program consists of a sandbox GUI where options can be controlled (sort of), so at launch time, any of these are optionnal.

#### dependencies :
- [SFML](https://www.sfml-dev.org/)
- [libosmium](https://osmcode.org/libosmium/)
- [jsoncpp](https://github.com/open-source-parsers/jsoncpp)
- [skeleton](https://github.com/nadous/skeleton)

#### TODO's:
- [ ] add a dependency to [NFD](https://github.com/mlabbe/nativefiledialog) to implement a cross platform solution for locating geojson file with a native file manager.

## Licence
[WTFPL](http://www.wtfpl.net/) – Do What the Fuck You Want to Public License.
